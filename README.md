# prgcmd

prgcmd is a GO library used to launch program's command by parsing command line.

## Command line format

"program name" [command] [arguments...] [flags...] [tail arguments...]

- command : name of command to execute
- arguments : argument value required by the command\
- flags : optional flag and value define by the command or as common for all commands
- tail arguments : arguments required define by the command or as common for all commands

If there is not arguments in the command line. It will display the help.

## Examples
```
./tool help connect
./tool connect 127.0.0.1 90 --ssh --timeout 100 -v
./tool list -v --max 100
./tool save -v C:\Windows
./tool load C:\Windows\File.txt -v C:\Temp
./tool load C:\Windows\File.txt C:\Temp
```

## Help commands
This command is used to display usage.

The format for help command is :
help [command]

If no command is provided, it will display the common usage.

```
.tool help
.tool help cmd1
```

## Sample of definition
```
type CommonArguments struct {
	Verbose bool `alias:"v" flag:"Be more verbose"`
}

type Command1 struct {
	Flag1 string `alias:"f" flag:"Flag that do something"`
	CommonArguments `name:"cmd1" prg_cmd:"Command #1"`
}

func (cmd *Command1) Run() error {
    /* ... */
	return nil
}

func main() {
	prg := Program{}
	prg.RegisterCommand(&Command1{})
	prg.RegisterDefaults(&CommonArguments{Verbose: true})
	if err := prg.Run() {
	    panic(err.Error())
	}
```

## Usage

First, you must declare a instance of prgcmd.Program instance.
```
prg := Program{}
```

Then, you must register commands and common arguments.

You register each command by calling RegisterCommand of your prgcmd.Program instance.

The method RegisterCommand required the pointer of an instance of the command struct with any default values.
```
prg.RegisterCommand(&Command1{Max: 100})
prg.RegisterCommand(&Command2{})
```

You register the common arguments by calling RegisterDefaults of your prgcmd.Program instance.

The method RegisterDefaults required the pointer of an instance of the common arguments struct with any default values.
```
prg.RegisterDefaults(&CommonArguments{Verbose: false})
```

**WARNING**
The common arguments instance **WILL NEVER** be filled by values in command line. It is the field identified as common arguments in the running command that will be used.

### Command

A common arguments struct must be define and present in all command structs even if there is no common arguments

To identify the common argument field, the Tag "prg_cmd" must be present. The value of this tag will be the help string display when usage is display.

The name of common arguements struct can be anything.

Common arguments struct:
```
type CommonArguments struct {
}
```

Declaration in command struct:
```
type Command1 struct {
	CommonArguments `prg_cmd:"Command #1"`
}
```

By default, the command name that would be used in command line is the structure name in lower case. But you can provide another name by adding the Tag "name".
```
type Command1 struct {
	CommonArguments `name:"cmd1" prg_cmd:"Command #1"`
}
```

### Command argument

Those arguments are mandatory and user most write values for them in the right order. Name of the argument should not be in command line.

```
./tool connect 127.0.0.1 80
```

Each field of the command struct with the Tag "argN" will be used to define arguments that must be present in the command line. N is the index of the argument.

The first argument must be 1.

And the sequence of argument must be complete. i.e. For a command with 4 arguments, arg1 to arg4 must be used. Can not be arg1 and arg12 to arg14.

By default, the command name that would be used in help is the structure field name in lower case. But you can provide another name by adding the Tag "name".

```
type Command1 struct {
	IP string `arg1:"IP Address"`
	IPPort int `name:"port" arg1:"Address port"`
	
	CommonArguments `name:"cmd1" prg_cmd:"Command #1"`
}
```

### Flags

Flag are optional argument and are optional. The basic format is --name value or --name for bool flag.

```
./tool list --verbose
./tool list --max 100
```

Flags can be common for all commands. In that case, you must add the field in your common arguments struct.

Each field of the command or common arguments struct with the Tag "flag" will be used to define flag that can be present in the command line.

By default, the flag name that would be used in command line and in help is the structure field name in lower case. But you can provide another name by adding the Tag "name".

You can add as shortcut by adding the Tag "alias". The format for alias is -alias value or -alias for bool flag.

```
./tool list -m 100
```

```
type Command1 struct {
	Verbose `flag:"Verbose flag"`
	Maximum int `alias:"m" flag:"Maximum value"`
	Minimum int `name:"min" flag:"Minimum value"`
	
	CommonArguments `name:"cmd1" prg_cmd:"Command #1"`
}
```

### Tail arguments

Tail arguments are mandatory arguments that will be after flags in the command line.

```
./tool list -m 100 C:\Windows
```

Tail arguments can be specific to commands or common to all commands.

To be common, tail arguments must be in common arguments struct.


Each field of the command struct with the Tag "argN" will be used to define tail arguments that must be present in the command line after flags. N is the index of the tail argument.

The first tail argument must be 1.

And the sequence of tail argument must be complete. i.e. For a command with 4 arguments, arg1 to arg4 must be used. Can not be arg1 and arg12 to arg14.

By default, the command name that would be used in help is the structure field name in lower case. But you can provide another name by adding the Tag "name".

If the command didn't have any tail arguments, the tail arguments from the common arguments will be used.

If you have tail arguments in both command and common arguments:
- The fail arguments in command will be used when using that command. Any of tail arguments in common arguments will be used
- When display common help, it is the tail arguments informations that will be display

```
type Command1 struct {
	Path string `tail1:"Path to use"`
	
	CommonArguments `name:"cmd1" prg_cmd:"Command #1"`
}

type CommonArguments struct {
	Path string `tail1:"Path to use in any command"`
}
```

### TO DO

- Add more supported data types for arguments and flags
- Display program title when print usage
- Display command title when print usage
- Add version as built-in command
