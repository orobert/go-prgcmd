package main

import (
	"fmt"
	"gitlab.com/orobert/go-prgcmd/prgcmd"
)

// CommonArguments must be exported
type CommonArguments struct {
	Verbose bool `alias:"v" flag:"be more verbose"`
}

// Connect must be exported
type Connect struct {
	Server string `arg1:"Server ip"`
	Port   int    `arg2:"Server ip port"`

	UseSSH  bool `name:"ssh" flag:"Use SSH"`
	Timeout int  `flag:"Timeout in seconds"`

	CommonArguments `prg_cmd:"command #1"`
}

// ListValues must be exported
type ListValues struct {
	Maximum int `name:"max" flag:"Maxmum number to display"`

	CommonArguments `name:"list" prg_cmd:"List values"`
}

// SaveCmd must be exported
type SaveCmd struct {
	Path string `tail1:"Path to use"`

	CommonArguments `name:"save" prg_cmd:"Save informations"`
}

// LoadCmd must be exported
type LoadCmd struct {
	File string `arg1:"File to use"`
	Path string `tail1:"Directory to use"`

	CommonArguments `name:"load" prg_cmd:"Load informations"`
}

// Run of Connect must be exported
func (cmd *Connect) Run() error {
	fmt.Println("Run Connect")
	fmt.Printf("Server: %s\n", cmd.Server)
	fmt.Printf("Port: %d\n", cmd.Port)
	fmt.Printf("UseSSH: %t\n", cmd.UseSSH)
	fmt.Printf("Timeout: %d\n", cmd.Timeout)
	fmt.Printf("Verbose: %t\n", cmd.Verbose)
	return nil
}

// Run of ListValues must be exported
func (cmd *ListValues) Run() error {
	fmt.Println("Run ListValues")
	fmt.Printf("Maximum: %d\n", cmd.Maximum)
	fmt.Printf("Verbose: %t\n", cmd.Verbose)
	return nil
}

// Run of SaveCmd must be exported
func (cmd *SaveCmd) Run() error {
	fmt.Println("Run SaveCmd")
	fmt.Printf("Path: %s\n", cmd.Path)
	fmt.Printf("Verbose: %t\n", cmd.Verbose)
	return nil
}

// Run of LoadCmd must be exported
func (cmd *LoadCmd) Run() error {
	fmt.Println("Run LoadCmd")
	fmt.Printf("File: %s\n", cmd.File)
	fmt.Printf("Path: %s\n", cmd.Path)
	fmt.Printf("Verbose: %t\n", cmd.Verbose)
	return nil
}

func main() {
	prg := prgcmd.Program{}
	prg.RegisterCommand(&Connect{})
	prg.RegisterCommand(&ListValues{Maximum: 1000})
	prg.RegisterCommand(&SaveCmd{})
	prg.RegisterCommand(&LoadCmd{})
	prg.RegisterDefaults(&CommonArguments{Verbose: false})

	err := prg.Run()
	if err != nil {
		panic(err.Error())
	}
}
