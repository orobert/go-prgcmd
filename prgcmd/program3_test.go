package prgcmd

import (
	"fmt"
	"testing"
)

type Prg3CommonArguments struct {
	Path0 string `tail1:"help info for tail argument path"`
}

type Prg3Command1 struct {
	Prg3CommonArguments `prg_cmd:"command #1"`

	Path1 string `tail1:"help info for tail argument path"`
}

type Prg3Command2 struct {
	Prg3CommonArguments `prg_cmd:"command #3"`
}

func (cmd *Prg3Command1) Run() error {
	fmt.Fprintln(&testOutputBuffer, "Prg3Command1")
	fmt.Fprintln(&testOutputBuffer, cmd.Path0)
	fmt.Fprintln(&testOutputBuffer, cmd.Path1)
	return nil
}

func (cmd *Prg3Command2) Run() error {
	fmt.Fprintln(&testOutputBuffer, "Prg3Command2")
	fmt.Fprintln(&testOutputBuffer, cmd.Path0)
	return nil
}

func TestProgram3(t *testing.T) {
	prg := Program{}
	prg.RegisterCommand(&Prg3Command1{})
	prg.RegisterCommand(&Prg3Command2{})
	prg.RegisterDefaults(&Prg3CommonArguments{})

	tests := []struct {
		name        string
		args        []string
		expectedErr string
	}{
		{
			name: "Usage",
			args: []string{},
		},
		{
			name: "Help command 1",
			args: []string{"help", "prg3command1"},
		},
		{
			name: "Help command 2",
			args: []string{"help", "prg3command2"},
		},
		{
			name: "Command 1",
			args: []string{"prg3command1", "c:\\windows"},
		},
		{
			name: "Command 2",
			args: []string{"prg3command2", "c:\\windows"},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			if err := doTest(prg, test.args, test.expectedErr, "Prg3", test.name); err != nil {
				t.Error(err)
			}
		})
	}
}
