Usage of testprg prg1command1: [flags...] path

arguments:

  path  help info for tail argument path

flags:

  --flag1, -f    flag that do something
  --max          maximum value
  --min, -m      minimum value
  --server, -s   string for server path
  --verbose, -v  be more verbose
  --w2           flag that do something
