package prgcmd

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"testing"
)

var testOutputBuffer bytes.Buffer

type Prg1CommonArguments struct {
	Verbose    bool   `alias:"v" flag:"be more verbose"`
	ServerPath string `name:"server" alias:"s" flag:"string for server path"`
	Min        int    `alias:"m" flag:"minimum value"`
	Max        int    `flag:"maximum value"`

	Path1 string `name:"path" tail1:"help info for tail argument path"`
}

type Prg1Command1 struct {
	Flag1 string  `alias:"f" flag:"flag that do something"`
	Flag2 float32 `name:"w2" flag:"flag that do something"`

	Prg1CommonArguments `prg_cmd:"command #1"`
}

type Prg1Command2 struct {
	Argument1 string `arg1:"value for 1st argument"`
	Argument2 string `name:"arg2" arg2:"value for 1st argument"`

	Prg1CommonArguments `name:"cmd2" prg_cmd:"command #2"`
}

func (cmd *Prg1Command1) Run() error {
	fmt.Fprintln(&testOutputBuffer, "Prg1Command1")
	fmt.Fprintln(&testOutputBuffer, cmd.Path1)
	fmt.Fprintln(&testOutputBuffer, cmd.ServerPath)
	fmt.Fprintln(&testOutputBuffer, cmd.Flag1)
	fmt.Fprintln(&testOutputBuffer, cmd.Min)
	fmt.Fprintln(&testOutputBuffer, cmd.Max)
	return nil
}

func (cmd *Prg1Command2) Run() error {
	fmt.Fprintln(&testOutputBuffer, "Prg1Command2")
	fmt.Fprintln(&testOutputBuffer, cmd.Path1)
	fmt.Fprintln(&testOutputBuffer, cmd.ServerPath)
	fmt.Fprintln(&testOutputBuffer, cmd.Argument1)
	fmt.Fprintln(&testOutputBuffer, cmd.Argument2)
	return nil
}

func TestProgram1(t *testing.T) {
	prg := Program{}
	prg.RegisterCommand(&Prg1Command1{})
	prg.RegisterCommand(&Prg1Command2{})
	prg.RegisterDefaults(&Prg1CommonArguments{Max: 100})

	tests := []struct {
		name        string
		args        []string
		expectedErr string
	}{
		{
			name: "Usage",
			args: []string{},
		},
		{
			name: "Simple help",
			args: []string{"help"},
		},
		{
			name: "Help for command 1",
			args: []string{"help", "prg1command1"},
		},
		{
			name: "Help for command 2",
			args: []string{"help", "cmd2"},
		},
		{
			name: "Call command 1",
			args: []string{"prg1command1", "--server", "127.0.0.1:1234", "-f", "abc", "-v", "-m", "-1", "c:\\windows"},
		},
		{
			name: "Call command 2 good",
			args: []string{"cmd2", "a", "b", "c:\\windows"},
		},
		{
			name:        "Call command 2 bad with flag",
			args:        []string{"cmd2", "a", "b", "--server", "127.0.0.1:1234"},
			expectedErr: "Missing argument path",
		},
		{
			name: "Call command 2 good with flag",
			args: []string{"cmd2", "a", "b", "--server", "127.0.0.1:1234", "c:\\windows"},
		},
		{
			name:        "Call command 2 bad with too many tail arguments",
			args:        []string{"cmd2", "a", "b", "c:\\windows", "aaa"},
			expectedErr: "Too many arguments",
		},
		{
			name:        "Call command 2 bad with too many cmd arguments",
			args:        []string{"cmd2", "a", "b", "aaa", "-v", "c:\\windows"},
			expectedErr: "Too many arguments",
		},
		{
			name:        "Call command 2 bad with too many cmd argument and no tail",
			args:        []string{"cmd2", "a", "b", "aaa", "-v"},
			expectedErr: "Too many arguments",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			if err := doTest(prg, test.args, test.expectedErr, "Prg1", test.name); err != nil {
				t.Error(err)
			}
		})
	}
}

func doTest(prg Program, args []string, expectedErr string, testSuite string, testName string) error {
	list := []string{"testprg"}
	list = append(list, args...)

	prg.SetOutput(&testOutputBuffer)

	testOutputBuffer.Reset()

	err := prg.RunArgs(list)
	if err != nil {
		errStr := err.Error()

		if expectedErr == "" || expectedErr != "" && expectedErr != errStr {
			return err
		}

		return nil

	} else if expectedErr != "" {
		return fmt.Errorf("Call must raise error \"%s\"", expectedErr)
	}

	testpath := filepath.Join(".", "tests", testSuite, testName+".txt")

	bytes, err := ioutil.ReadFile(testpath)
	if err != nil {
		return err
	}

	if testOutputBuffer.String() != string(bytes) {
		return fmt.Errorf("Output isn't what is expected")
	}

	return nil
}
