package prgcmd

import (
	"fmt"
	"reflect"
	"strconv"
)

type valueField struct {
	name       string
	field      reflect.StructField
	fieldValue reflect.Value
	index      int
}

func newValueField(v reflect.Value, t reflect.Type, i int) *valueField {

	f := t.Field(i)

	return &valueField{name: f.Name, field: f, fieldValue: v.Field(i), index: i}
}

func (f *valueField) Validate() {
	switch f.fieldValue.Addr().Interface().(type) {
	case *bool, *float32, *float64, *int, *int64, *string:
		return
	}

	panic("Unsupported type")
}

func (f *valueField) Update(defaults interface{}) {

	v := reflect.ValueOf(defaults)
	if v.Kind() != reflect.Ptr || v.IsNil() {
		panic("Invalid argument")
	}

	v = v.Elem()
	t := v.Type()

	defaultValue := f.fieldValue.Interface()
	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}

	f.field = t.Field(f.index)
	f.fieldValue = v.Field(f.index)

	f.fieldValue.Set(reflect.ValueOf(defaultValue))
}

func (f *valueField) GetTag(name string) string {
	return f.field.Tag.Get(name)
}

func (f *valueField) IsBool() bool {
	return f.field.Type.Kind() == reflect.Bool
}

func (f *valueField) SetValueFromString(value string) error {

	switch f.fieldValue.Addr().Interface().(type) {
	case *bool:
		f.fieldValue.SetBool(value == "1")
	case *float64:
		if fl, err := strconv.ParseFloat(value, 64); err == nil {
			f.fieldValue.SetFloat(fl)
		} else {
			return err
		}
	case *int:
		if i, err := strconv.ParseInt(value, 10, 32); err == nil {
			f.fieldValue.SetInt(i)
		} else {
			return err
		}
	case *int64:
		if i, err := strconv.ParseInt(value, 10, 64); err == nil {
			f.fieldValue.SetInt(i)
		} else {
			return err
		}
	case *string:
		f.fieldValue.SetString(value)
	}

	return fmt.Errorf("Type not supported")
}
