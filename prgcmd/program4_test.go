package prgcmd

import (
	"fmt"
	"testing"
)

type Prg4CommonArguments struct {
}

type Prg4Command1 struct {
	Argument1 string `arg1:"value for 1st argument"`

	Flag1 string `alias:"f" flag:"flag that do something"`

	Prg4CommonArguments `prg_cmd:"command #1"`
}

func (cmd *Prg4Command1) Run() error {
	fmt.Fprintln(&testOutputBuffer, "Prg4Command1")
	fmt.Fprintln(&testOutputBuffer, cmd.Argument1)
	fmt.Fprintln(&testOutputBuffer, cmd.Flag1)
	return nil
}

func TestProgram4(t *testing.T) {
	prg := Program{}
	prg.RegisterCommand(&Prg4Command1{})
	prg.RegisterDefaults(&Prg4CommonArguments{})

	tests := []struct {
		name        string
		args        []string
		expectedErr string
	}{
		{
			name: "Usage",
			args: []string{},
		},
		{
			name: "Help of prg4command1",
			args: []string{"help", "prg4command1"},
		},
		{
			name:        "Call of Prg4Command1 0 arg",
			args:        []string{"prg4command1"},
			expectedErr: "Missing argument argument1",
		},
		{
			name: "Call of Prg4Command1 1 arg",
			args: []string{"prg4command1", "aaa"},
		},
		{
			name:        "Call of Prg4Command1 2 arg",
			args:        []string{"prg4command1", "aaa", "bbb"},
			expectedErr: "Too many arguments",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			if err := doTest(prg, test.args, test.expectedErr, "Prg4", test.name); err != nil {
				t.Error(err)
			}
		})
	}
}
