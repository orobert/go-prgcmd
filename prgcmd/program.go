package prgcmd

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"reflect"
	"sort"
	"strings"
	"text/tabwriter"
)

// Program : struct to handle launch of a program using command line
type Program struct {
	prgName  string
	commands map[string]command

	defaults      interface{}
	tailArguments []argument
	flags         map[string]flag
	aliasFlags    map[string]flag

	output io.Writer

	anyFlags            bool
	anyCmdArguments     bool
	anyCmdTailArguments bool
}

type argument struct {
	name          string
	help          string
	field         *valueField
	argumentIndex int
}

type flag struct {
	name   string
	alias  string
	help   string
	field  *valueField
	isBool bool
}

// SetOutput : set output stream
// default is stderr
func (prg *Program) SetOutput(output io.Writer) {
	prg.output = output
}

// RegisterCommand : register a command
// cmd : pointer to instance of argument defaults of the command
func (prg *Program) RegisterCommand(cmd interface{}) {
	prg.init()

	v := reflect.ValueOf(cmd)
	if v.Kind() != reflect.Ptr || v.IsNil() {
		panic("Invalid argument")
	}

	v = v.Elem()

	c := command{}
	c.flags = make(map[string]flag)
	c.aliasFlags = make(map[string]flag)

	t := v.Type()
	for i := 0; i < t.NumField(); i++ {

		field := newValueField(v, t, i)

		isArg := false
		for argIndex := 1; argIndex < 10; argIndex++ {
			name := fmt.Sprintf("arg%d", argIndex)
			if a, ok := newArgument(name, argIndex, field); ok {
				c.cmdArguments = append(c.cmdArguments, a)

				isArg = true
				break
			}

			name = fmt.Sprintf("tail%d", argIndex)
			if a, ok := newArgument(name, argIndex, field); ok {
				c.tailArguments = append(c.tailArguments, a)

				isArg = true
				break
			}
		}

		if isArg {
			continue
		}

		if s := field.GetTag("prg_cmd"); s != "" {

			help := s
			name := strings.ToLower(t.Name())

			if s := field.GetTag("name"); s != "" {
				name = s
			}

			c.name = name
			c.help = help
			c.arguments = cmd
			c.commonArguments = v.Field(i).Addr().Interface()

		} else if f, ok := newFlag(field); ok {
			c.flags[f.name] = f
			if f.alias != "" {
				c.aliasFlags[f.alias] = f
			}
		}
	}

	if c.name == "" {
		panic("By design, we need one 'common arguments' field even if is empty")
	}

	if err := validateArguments(c.cmdArguments); err != nil {
		panic(err.Error)
	}
	if err := validateArguments(c.tailArguments); err != nil {
		panic(err.Error())
	}

	prg.commands[c.name] = c
}

// RegisterDefaults : register common defaults
// defaults : pointer to instance of common argument defaults
func (prg *Program) RegisterDefaults(defaults interface{}) {
	prg.init()

	prg.defaults = defaults
	prg.tailArguments = []argument{}

	v := reflect.ValueOf(defaults)
	if v.Kind() != reflect.Ptr || v.IsNil() {
		panic("Invalid argument")
	}

	v = v.Elem()
	t := v.Type()

	for i := 0; i < t.NumField(); i++ {
		field := newValueField(v, t, i)
		isArg := false
		for argIndex := 1; argIndex < 10; argIndex++ {
			name := fmt.Sprintf("tail%d", argIndex)
			if a, ok := newArgument(name, argIndex, field); ok {
				prg.tailArguments = append(prg.tailArguments, a)

				isArg = true
				break
			}
		}

		if isArg {
			continue
		}

		if f, ok := newFlag(field); ok {
			prg.flags[f.name] = f
			if f.alias != "" {
				prg.aliasFlags[f.alias] = f
			}
		}
	}

	if err := validateArguments(prg.tailArguments); err != nil {
		panic(err.Error())
	}
}

// Run : parse command line and execute command or display help / usage
func (prg *Program) Run() error {
	return prg.RunArgs(os.Args)
}

// RunArgs : parse command line from arguments list and execute command or display help / usage
func (prg *Program) RunArgs(args []string) error {
	if prg.commands == nil {
		panic("No command registered")
	}
	if prg.defaults == nil {
		panic("No arguments defaults registered")
	}

	prg.determineAnyFlagsArguments()

	prg.prgName = filepath.Base(args[0])

	args = args[1:]

	if len(args) == 0 {
		return prg.printUsage()
	}

	cmdName := args[0]
	args = args[1:]
	helpCmd := false

	if cmdName == "help" {
		if len(args) == 0 {
			return prg.printUsage()
		}

		helpCmd = true
		cmdName = args[0]
	}

	cmd, ok := prg.commands[cmdName]
	if !ok {
		return fmt.Errorf("Command '%s' don't exist", cmdName)
	}

	if helpCmd {
		return prg.printCommandUsage(cmd)
	}

	if err := cmd.parseCommandLine(prg, args); err != nil {
		return err
	}

	type caller interface {
		Run() error
	}

	runCaller, ok := cmd.arguments.(caller)
	if !ok {
		panic("No entry point for " + cmdName)
	}

	return runCaller.Run()
}

func (prg *Program) init() {
	if prg.commands != nil {
		return
	}

	prg.commands = make(map[string]command)
	prg.output = os.Stderr

	prg.flags = make(map[string]flag)
	prg.aliasFlags = make(map[string]flag)
}

func (prg *Program) determineAnyFlagsArguments() {
	prg.anyFlags = false
	prg.anyCmdArguments = false
	prg.anyCmdTailArguments = false

	if len(prg.flags) > 0 {
		prg.anyFlags = true
	}

	for _, cmd := range prg.commands {
		if len(cmd.flags) > 0 {
			prg.anyFlags = true
		}
		if len(cmd.tailArguments) > 0 {
			prg.anyCmdTailArguments = true
		}
		if len(cmd.cmdArguments) > 0 {
			prg.anyCmdArguments = true
		}
	}
}

func (prg *Program) printUsage() error {
	fmt.Fprintf(prg.output, "Usage of %s: command", prg.prgName)
	if prg.anyCmdArguments {
		fmt.Fprintf(prg.output, " [arguments...]")
	}
	if prg.anyFlags {
		fmt.Fprintf(prg.output, " [flags...]")
	}
	if len(prg.tailArguments) > 0 {
		for _, arg := range prg.tailArguments {
			fmt.Fprintf(prg.output, " %s", arg.name)
		}
	} else if prg.anyCmdTailArguments {
		fmt.Fprintf(prg.output, " [arguments...]")
	}
	fmt.Fprintln(prg.output, "")

	fmt.Fprintln(prg.output, "")
	fmt.Fprintln(prg.output, "commands:")
	fmt.Fprintln(prg.output, "")
	prg.printCommands()

	if len(prg.tailArguments) > 0 {
		fmt.Fprintln(prg.output, "")
		fmt.Fprintln(prg.output, "arguments:")
		fmt.Fprintln(prg.output, "")
		prg.printArguments(prg.tailArguments)
	}

	if len(prg.flags) > 0 {
		fmt.Fprintln(prg.output, "")
		fmt.Fprintln(prg.output, "flags:")
		fmt.Fprintln(prg.output, "")
		prg.printFlags(prg.flags)
	}

	return nil
}

func (prg *Program) printCommandUsage(cmd command) error {

	flags := mergeFlags(prg.flags, cmd.flags)

	tailArguments := selectTailArguments(prg.tailArguments, cmd.tailArguments)

	fmt.Fprintf(prg.output, "Usage of %s %s:", prg.prgName, cmd.name)
	for _, arg := range cmd.cmdArguments {
		fmt.Fprintf(prg.output, " %s", arg.name)
	}
	if len(flags) > 0 {
		fmt.Fprintf(prg.output, " [flags...]")
	}

	for _, arg := range tailArguments {
		fmt.Fprintf(prg.output, " %s", arg.name)
	}
	fmt.Fprintln(prg.output, "")

	if len(cmd.cmdArguments) > 0 || len(tailArguments) > 0 {
		fmt.Fprintln(prg.output, "")
		fmt.Fprintln(prg.output, "arguments:")
		if len(cmd.cmdArguments) > 0 {
			fmt.Fprintln(prg.output, "")
			prg.printArguments(cmd.cmdArguments)
		}
		if len(tailArguments) > 0 {
			fmt.Fprintln(prg.output, "")
			prg.printArguments(tailArguments)
		}
	}

	if len(flags) > 0 {
		fmt.Fprintln(prg.output, "")
		fmt.Fprintln(prg.output, "flags:")
		fmt.Fprintln(prg.output, "")
		prg.printFlags(flags)
	}

	return nil
}

func (prg *Program) printCommands() {
	list := []string{}

	add := func(key, help string) {
		list = append(list, fmt.Sprintf("  %s\t%s", key, help))
	}

	for _, item := range prg.commands {
		add(item.name, item.help)
	}

	add("help", "show help on available commands")

	prg.printList(list, true)
}

func (prg *Program) printList(list []string, sorting bool) {

	if sorting {
		sort.Strings(list)
	}

	w := tabwriter.NewWriter(prg.output, 0, 0, 2, ' ', 0)

	for _, s := range list {
		fmt.Fprintln(w, s)
	}

	w.Flush()
}

func (prg *Program) printArguments(args []argument) {
	list := []string{}

	for _, arg := range args {
		list = append(list, fmt.Sprintf("  %s\t%s", arg.name, arg.help))
	}

	prg.printList(list, false)
}

func (prg *Program) printFlags(flags map[string]flag) {
	list := []string{}

	for _, f := range flags {
		str := ""
		if f.alias != "" {
			str = fmt.Sprintf("  --%s, -%s\t%s", f.name, f.alias, f.help)
		} else {
			str = fmt.Sprintf("  --%s\t%s", f.name, f.help)
		}
		list = append(list, str)
	}

	prg.printList(list, true)
}

func newArgument(tag string, argumentIndex int, field *valueField) (argument, bool) {
	if s := field.GetTag(tag); s != "" {
		field.Validate()

		help := s
		name := strings.ToLower(field.name)

		if s := field.GetTag("name"); s != "" {
			name = s
		}

		a := argument{
			name:          name,
			help:          help,
			field:         field,
			argumentIndex: argumentIndex,
		}
		return a, true
	}

	return argument{}, false
}

func newFlag(field *valueField) (flag, bool) {
	if s := field.GetTag("flag"); s != "" {
		field.Validate()

		help := s
		name := strings.ToLower(field.name)
		alias := ""

		if s := field.GetTag("name"); s != "" {
			name = s
		}

		if s := field.GetTag("alias"); s != "" {
			alias = s
		}

		f := flag{
			name:  name,
			alias: alias,
			help:  help,
			field: field,
		}

		if field.IsBool() {
			f.isBool = true
		}

		return f, true
	}

	return flag{}, false
}

func mergeFlags(m1 map[string]flag, m2 map[string]flag) map[string]flag {
	m := make(map[string]flag)

	for key, f := range m1 {
		m[key] = f
	}
	for key, f := range m2 {
		m[key] = f
	}

	return m
}

func validateArguments(list []argument) error {
	sort.SliceStable(list, func(i, j int) bool {
		return list[i].argumentIndex < list[j].argumentIndex
	})

	for i, a := range list {
		argumentIndex := i + 1
		if a.argumentIndex != argumentIndex {
			return fmt.Errorf("Missing argument #%d", argumentIndex)
		}
	}

	return nil
}

func selectTailArguments(prg []argument, cmd []argument) []argument {
	if len(cmd) > 0 {
		return cmd
	}

	return prg
}
