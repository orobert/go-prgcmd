package prgcmd

import (
	"fmt"
	"strings"
)

type command struct {
	name string
	help string

	cmdArguments  []argument
	tailArguments []argument

	flags      map[string]flag
	aliasFlags map[string]flag

	arguments       interface{}
	commonArguments interface{}
}

func (cmd *command) parseCommandLine(prg *Program, args []string) error {

	cmd.updateFlagsArgument(prg)

	tail := cmd.tailArguments
	if len(tail) == 0 {
		tail = prg.tailArguments
	}

	if len(args) < len(cmd.cmdArguments) {
		return fmt.Errorf("Missing argument %s", cmd.cmdArguments[len(args)].name)
	}

	for i := 0; i < len(cmd.cmdArguments); i++ {
		cmd.cmdArguments[i].field.SetValueFromString(args[i])
	}

	args = args[len(cmd.cmdArguments):]

	i := 0
	for ; i < len(args); i++ {

		arg := args[i]

		if strings.HasPrefix(arg, "-") {
			// is a flag
			name := arg[1:]

			if flag, found := cmd.getFlag(prg, name); found {
				if flag.isBool {
					flag.field.SetValueFromString("1")
				} else {
					i++
					if i >= len(args) {
						return fmt.Errorf("Missing value for flag -%s", name)
					}
					value := args[i]

					flag.field.SetValueFromString(value)
				}
			}

		} else {
			break
		}
	}
	args = args[i:]

	if len(args) < len(tail) {
		return fmt.Errorf("Missing argument %s", tail[len(args)].name)
	} else if len(args) > len(tail) {
		return fmt.Errorf("Too many arguments")
	}

	for i := 0; i < len(args); i++ {
		tail[i].field.SetValueFromString(args[i])
	}

	return nil
}

func (cmd *command) getFlag(prg *Program, name string) (flag, bool) {

	var commonMap map[string]flag
	var cmdMap map[string]flag

	if strings.HasPrefix(name, "-") {
		// full length name
		name = name[1:]

		commonMap = prg.flags
		cmdMap = cmd.flags

	} else {
		// alias

		commonMap = prg.aliasFlags
		cmdMap = cmd.aliasFlags
	}

	// first search in command flags
	if f, ok := cmdMap[name]; ok {
		return f, true
	}

	// if not found, search in common flags
	if f, ok := commonMap[name]; ok {
		return f, true
	}

	return flag{}, false
}

func (cmd *command) updateFlagsArgument(prg *Program) {

	for _, flag := range prg.flags {
		flag.field.Update(cmd.commonArguments)
	}

	for _, arg := range prg.tailArguments {
		arg.field.Update(cmd.commonArguments)
	}
}
