package prgcmd

import (
	"fmt"
	"testing"
)

type Prg2CommonArguments struct {
}

type Prg2Command1 struct {
	Prg1CommonArguments `prg_cmd:"command #1"`

	Path1 string `name:"path" tail1:"help info for tail argument path"`
}

type Prg2Command2 struct {
	Prg2CommonArguments `prg_cmd:"command #2"`

	Path1 string `name:"path" tail1:"help info for tail argument path"`
	Path2 string `name:"subpath" tail2:"help info for tail argument subpath"`
}

type Prg2Command3 struct {
	Prg2CommonArguments `prg_cmd:"command #3"`
}

func (cmd *Prg2Command1) Run() error {
	fmt.Fprintln(&testOutputBuffer, "Prg2Command1")
	fmt.Fprintln(&testOutputBuffer, cmd.Path1)
	return nil
}

func (cmd *Prg2Command2) Run() error {
	fmt.Fprintln(&testOutputBuffer, "Prg2Command2")
	fmt.Fprintln(&testOutputBuffer, cmd.Path1)
	fmt.Fprintln(&testOutputBuffer, cmd.Path2)
	return nil
}

func (cmd *Prg2Command3) Run() error {
	fmt.Fprintln(&testOutputBuffer, "Prg2Command3")
	return nil
}

func TestProgram2(t *testing.T) {

	prg := Program{}
	prg.RegisterCommand(&Prg2Command1{})
	prg.RegisterCommand(&Prg2Command2{})
	prg.RegisterCommand(&Prg2Command3{})
	prg.RegisterDefaults(&Prg2CommonArguments{})

	tests := []struct {
		name        string
		args        []string
		expectedErr string
	}{
		{
			name: "Usage",
			args: []string{},
		},
		{
			name: "Help Command 1",
			args: []string{"help", "prg2command1"},
		},
		{
			name: "Help Command 2",
			args: []string{"help", "prg2command2"},
		},
		{
			name: "Help Command 3",
			args: []string{"help", "prg2command3"},
		},
		{
			name: "Command 1 good",
			args: []string{"prg2command1", "c:\\windows"},
		},
		{
			name:        "Command 1 bad no argument",
			args:        []string{"prg2command1"},
			expectedErr: "Missing argument path",
		},
		{
			name:        "Command 1 bad 2 arguments",
			args:        []string{"prg2command1", "c:\\windows", "aaa"},
			expectedErr: "Too many arguments",
		},
		{
			name: "Command 2 good",
			args: []string{"prg2command2", "c:\\windows", "drivers"},
		},
		{
			name:        "Command 2 bad 1 argument",
			args:        []string{"prg2command2", "c:\\windows"},
			expectedErr: "Missing argument subpath",
		},
		{
			name:        "Command 2 bad 3 arguments",
			args:        []string{"prg2command2", "c:\\windows", "drivers", "aaa"},
			expectedErr: "Too many arguments",
		},
		{
			name: "Command 3 good no argument",
			args: []string{"prg2command3"},
		},
		{
			name:        "Command 3 bad 1 argument",
			args:        []string{"prg2command3", "c:\\windows"},
			expectedErr: "Too many arguments",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			if err := doTest(prg, test.args, test.expectedErr, "Prg2", test.name); err != nil {
				t.Error(err)
			}
		})
	}
}
